# K3S and K3D for Local Application Development Environment

### K3S Introduction:

K3S is Lightweight Kubernetes. For local kubernetes based development, we are selecting k3s over [minikube](https://github.com/kubernetes/minikube). 
Kubernetes on laptop needs minimum 2 CPUs and 2000 MB Memory (considering minikube). To avoid such resource contraint, we are going to use [k3s](https://github.com/rancher/k3s) which is the lightweight Kubernetes distribution by Rancher.
On MacOS we are using homebrew to install k3s/k3d. We are using k3d which is little helper to run k3s in Docker. In other words, we are using k3d command which will create k3s custer (one master node and two worker node) and it will interact with dockers which will having all required files and working kubernetes system.

### k3d commands

- k3s install
Create Cluster:
```k3d create --publish 80:80 --workers 2```

Set kubeconfig environment vaiable, so kubectl on local system can control k3s cluster.
```export KUBECONFIG="$$(k3d get-kubeconfig --name='k3s-default')"```

- k3s test 
Following commands just test basic cluster.

List all cluster:
```k3d list```

Print k3d version:
```k3d -v```

Check if docker is running:
```k3d check-tools```

- k3s uninstall
Stop and delete the cluster:
```k3d stop && k3d delete```

Using homebrew uninstall k3d:
```brew uninstall k3d```

### Web services demo

- k3s web services demo
  - Use ingress
    Uinsg ```ingress.yaml```, user can provide multiple path to access respective service.

  - Check access
    Create k3s objects like nginx, httpd deployment and service:
    ```make create_demo1```
    ```make demo1```
    ```make demo1_service``` 

  - Check given service
    Using host and path user can access given service (at specific port number).
    We have configured following domains which resolved at 127.0.0.1.

    Name | Useage
    -----|------
    devlocal_frontenddb.gpls.pro| Frontend Database service
    devlocal_frontendweb.gpls.pro | Frontend Web service
    devlocal_frontendapi.gpls.pro | Frontend API service

    PS: These DNS hostnames resolves to 127.0.0.1. Using ```nslookup``` you can check on your Mac System, if you have any question then please contact DevOps team on *Slack*.

### Database Service demo

- k3s mysql service demo
Using MySQL 5.6 docker image from [hub.docker.com](https://hub.docker.com/_/mysql), we have created mysql deployment and service.

```
name=mysql
kubectl run ${name} --image mysql:5.6 --port=3306  --env=MYSQL_USER=user --env=MYSQL_PASSWORD=pass --env=MYSQL_DATABASE=testdb --env=MYSQL_ROOT_PASSWORD=pass
kubectl expose deployment ${name}  --port=3306 --target-port=3306; 
```
In above command, we have passed environment variables to set in the container. 

To access mysql service, we used ```kubectl port-forward``` command. Which forward one or more local ports to a pod/deployment/service.
```kubectl port-forward service/mysql 3306 &```
Above command will listen on port 3306 locally, forwarding data to/from ports 3306 in a pod selected by the mysql service. i.e. it will listen on localhost (127.0.0.1) on port 3306 (MySQL Port) and all the traffic will be forwarded to mysql service (and its pod) on port 3306.

```mysql -uuser -ppass  -h 127.0.0.1 testdb -e "use testdb;select database();"```
Above command will connect MySQL server using given username and password, which is running as *mysql service*. User have to install mysql client. Using brew - ```brew install mysql-client@5.7```.




create_demo1:
	kubectl apply -f src/demo1/ingress.yaml
	sh src/demo1/deploy.sh

delete_demo1:
	kubectl delete -f src/demo1/ingress.yaml
	sh src/demo1/delete.sh

demo1:
	@echo "\nFollowing is Nginx Server (port 80), will successfully accept / as path. \n"
	curl -sI devlocal_frontenddb.gpls.pro | head -5
	@echo "\n\nFollowing is Nginx Server (port 81), will fail to accept anything other than "/" as path. \n"
	curl -sI devlocal_frontenddb.gpls.pro/nginx1 | head -5
	@echo "\n\nFollowing is Apache2 server on port 82. Which can only accept /index.html as path. \n"
	curl -sI devlocal_frontenddb.gpls.pro/index.html | head -5
	@echo "\n\nFollowing is echoserver which can accept any path (or end point). \n "
	curl -sI devlocal_frontenddb.gpls.pro/echo2 | head -5

demo1_service:
	@echo "\nFollowing is Nginx Server (port 80), will successfully accept / as path. \n"
	curl -s devlocal_frontendweb.gpls.pro | head -5
	@echo "\n\nFollowing is Nginx Server (port 81), will fail to accept anything other than "/" as path. \n"
	curl -s devlocal_frontendweb.gpls.pro/nginx1 | head -5
	@echo "\n\nFollowing is Apache2 server on port 82. Which can only accept /index.html as path. \n"
	curl -s devlocal_frontendweb.gpls.pro/index.html | head -5
	@echo "\n\nFollowing is echoserver which can accept any path (or end point). \n "
	curl -s devlocal_frontendapi.gpls.pro/echo2 | head -5

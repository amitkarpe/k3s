
create_demo2:
	sh src/demo2/deploy.sh
	pkill kubectl | true
	@kubectl get pods -l "run=mysql" -o jsonpath="{.items[0].metadata.name}"
	@echo "\n\n"
	#export POD_NAME=$$(kubectl get pods -l "run=mysql" -o jsonpath="{.items[0].metadata.name}"); echo $${POD_NAME}; kubectl port-forward $$POD_NAME 3306:3306 &
	kubectl port-forward service/mysql 3306:3306 &

delete_demo2:
	sh src/demo2/delete.sh

demo2:
#	export test=foo; echo $${test};	echo $$test
	@sleep 1
	mysql -uuser -ppass  -h 127.0.0.1 testdb -e "use testdb;select database();"
	@echo "\n"
#	mysql -uuser -ppass -h devlocal_frontenddb.gpls.pro testdb -e "use testdb; select database();"

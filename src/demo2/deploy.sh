#!/bin/bash


kubectl get svc,deploy,ingress
name=mysql
kubectl run ${name} --image mysql:5.6 --port=3306  --env=MYSQL_USER=user --env=MYSQL_PASSWORD=pass --env=MYSQL_DATABASE=testdb --env=MYSQL_ROOT_PASSWORD=pass
kubectl expose deployment ${name}  --port=3306 --target-port=3306; 

sleep 10;
kubectl get pod 
#kubectl get pods -l "run=mysql" -o jsonpath="{.items[0].metadata.name}"
#export POD_NAME=$(kubectl get pods -l "run=mysql" -o jsonpath="{.items[0].metadata.name}")
kubectl port-forward $(kubectl get pods -l "run=mysql" -o jsonpath="{.items[0].metadata.name}") 3306:3306 &

#kubectl port-forward $POD_NAME 3306:3306 &

sleep 5;
kubectl get svc,deploy,ingress
kubectl get endpoints

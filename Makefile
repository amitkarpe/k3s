include demo1.Makefile
include demo2.Makefile

test:
	k3d list
	k3d -v
	k3d check-tools


setup: install test cluster

install:
	docker info | head
	brew install k3d
	k3d create --publish 80:80 --workers 2;
	export KUBECONFIG="$$(k3d get-kubeconfig --name='k3s-default')"
	
cluster:
	kubectl cluster-info
	kubectl get nodes
	kubectl config get-contexts
	kubectl config view --minify

uninstall:
	k3d stop && k3d delete
	brew uninstall k3d
	
